# Git

## Iniciar git en un proyecto

```
git init
```

## Clonar un proyecto

```
git clone <url>
```

## Añadir a staging area
Un solo fichero:
```
git add <fichero>
```
Añadir todo el directorio al staging area

```
git add .
```

## Ver modificaciones y archivos modificados
### Ver ficheros modificados
```
git status
```
De forma esquematizada:
```
git status -s
```
### Ver cambio en el contenido de los ficheros

```
git diff
```
## Variables de usuario

```
git config --global user.name "<nombre>"
git config --global user.email "<email>"
```

## Commit
```
git commit -m "<mensaje>"
```
## Branches
### Crear una rama
```
git branch <nombre de rama>
```
### Cambiar de rama
```
git checkout <nombre de rama>
```
## Merge
```
git merge <rama origen> <rama donde quiero hacer el merge>
```