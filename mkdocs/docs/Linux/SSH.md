# SSH

## SSH Config

### ProxyCommand
Used to configure bastion or jumphost instances.

```
Host proxy
  Hostname <Proxy IP>
  User <user>
  Port <port>
  IdentityFile <Path to Proxy Host Identity File>

Host HostBehindProxy
  Hostname <HostBehindProxy IP>
  User <user>
  Port <port>
  IdentityFile <Path to HostBehindProxy Identity File>
  ProxyCommand ssh proxy -W %h:%p

```


## SSH Keys

### Get SSH key fingerprint

```
openssl pkcs8 -in <key> -inform PEM -outform DER -topk8 -nocrypt | openssl sha1 -c
```
