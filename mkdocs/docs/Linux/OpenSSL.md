# OpenSSL

## SSL client

### Connect to SSL endpoint
Used to configure bastion or jumphost instances.

```
openssl s_client -connect <host:port>
```
